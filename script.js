let canvas = document.getElementById('canvas');
let context= canvas.getContext('2d');

document.body.onkeydown = control;

let scoresElem = document.querySelector("h1 > span");
let scores = 0;

let highScores = [
    {name:"John", scores:1000},
    {name:"Peter", scores:90},
    {name:"Mike", scores:1},
];



let snake ={
    direction:39,
    speed :1000,
    color:"#546DEA",
    strokeColor:"#fff",
    width:50,
    pause: false,
     x: [100,50,0],
     y: [0,0,0],
};

let meadowImg = document.createElement("img");
meadowImg.src = "Meadow.png";

let rabbitImg = document.createElement('img');
rabbitImg.src = "https://pngimg.com/uploads/rabbit/rabbit_PNG3804.png";


class Meadow {
  constructor(image, x, y) {
    this.image = image;
    this.x = x;
    this.y = y;
  }

  render(){
    context.drawImage(this.image, this.x, this.y, this.image.width, this.image.height);
  }
}

let meadows = [];
meadowImg.onload = meadowCreate.bind(meadowImg);
function meadowCreate() {
    for (var i = 0; i < Math.ceil(canvas.width/this.width); i++) {
        for (var j = 0; j < Math.ceil(canvas.height/this.height); j++) {
            meadows.push(new Meadow(meadowImg, i * this.width, j * this.height));
        }
    }
}

function Rabbit(){
    this.width =50;
    this.image = rabbitImg;
    this.x = Math.floor(Math.floor(Math.random()*canvas.width) / this.width) * this.width;
    this.y = Math.floor(Math.floor(Math.random()*canvas.height) / this.width) *this.width;
}

Rabbit.prototype.render = function(){
  context.drawImage(this.image, this.x, this.y, this.width, this.width);
}

let rabbit = new Rabbit();

context.fillStyle = snake.color;
context.srokeStyle = snake.strokeColor;

function eat(){
  rabbit = null;
  scores++;
}

function collision(){
  if (
    snake.x[0] < 0 ||
    snake.x[0] > canvas.width - snake.width ||
    snake.y[0] < 0 ||
    snake.y[0] > canvas.height - snake.width
  ) {
    gameOver();
  }

  for (var i = 1; i < snake.x.length; i++) {
    if (
      snake.x[i] == snake.x[0] &&
      snake.y[i] == snake.y[0]
    ) {
        gameOver();
    }
  }
}

function gameOver(){
  stop();
  let table = document.querySelector("table");
  let name = prompt("Введите своё имя");
  if (name){
      highScores.push({name,scores});
  }
  highScores.sort((a,b)=>a.scores > b.scores ? 1 : -1);

  table.innerHTML = highScores.map((highScore)=>`<tr><td>${highScore.name}</td><td>${highScore.scores}</td></tr>`).join("");

}


function control(event){
    let code = event.keyCode;
    snake.direction = (
        [37,39].includes(code) &&
        [38,40].includes(snake.direction)
    ) ? code: snake.direction;
    snake.direction = (
        [38,40].includes(code) &&
        [37,39].includes(snake.direction)
    ) ? code: snake.direction;
    if (code == 32) snake.pause = !snake.pause;
}

function move(){
    if (snake.y[0] == rabbit.y && snake.x[0] == rabbit.x) {
      eat();
    }else {
      let a = snake.x.pop();
      let b = snake.y.pop();
    }
      switch (snake.direction) {
        case 39://right
        snake.x.unshift(snake.x[0] + snake.width);
        snake.y.unshift(snake.y[0]);
        break;
        case 37://left
        snake.x.unshift(snake.x[0] - snake.width);
        snake.y.unshift(snake.y[0]);
        break;
        case 38://up
        snake.y.unshift(snake.y[0] - snake.width);
        snake.x.unshift(snake.x[0]);
        break;
        case 40://down
        snake.y.unshift(snake.y[0] + snake.width);
        snake.x.unshift(snake.x[0]);
        break;
        default:
        snake.x.push(a);
        snake.y.push(b);
      }
}


function draw(){
  // Очищаем текущий кадр
    context.clearRect(0,0,canvas.width,canvas.height);
    // Рисуем лужайку
    for (let tile of meadows) {
      tile.render();
    }
    // Рисуем кролика
    rabbit.render();
      // рисуем змейку
    for (var i = 0; i < snake.x.length; i++) {
        context.fillRect(snake.x[i],snake.y[i],snake.width,snake.width);
        context.strokeRect(snake.x[i],snake.y[i],snake.width,snake.width);
    }

    scoresElem.textContent = scores;
}


let time;
function start() {
  time = setInterval(
    ()=>{if (!snake.pause) {
        collision();
        move();

        if (!rabbit) {
            rabbit = new Rabbit();
        }
        draw();
    }},
    snake.speed
  );
}

function stop(){
  clearInterval(time);
}

start();
